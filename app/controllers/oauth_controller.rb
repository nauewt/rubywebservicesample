class OauthController < ApplicationController
  def callback
  	client = OAuth2::Client.new('ABCDCLIENTID', 'ABCDCLIENTSECRET', 
	  		:site => 'https://oleadmin.ucc.nau.edu',
	  		:authorize_url		=> "/api/oauth2/auth",
	  		:token_url		=> "/api/oauth2/token")
  	if not params[:code].blank?
	    ## Should only get this once. Save this token. 
	    token = client.auth_code.get_token(params[:code])
	    ## After you have this token, check for expired and refresh if needed.
	    if token.expired?
	    	token.refresh!
	    end
	    ## With this token, you can make API requests. Pass params if needed as shown below.
	    response = token.get('https://oleadmin.ucc.nau.edu/api/user_session', :params => { 'just_a' => 'test_not_used' })

	    ## Do stuff with this data.
	    @response = response.parsed
	    @expires = token.expires?
	else
		## Since this page does both the callback and runs a request, do it all here. No code? Go get one.
		redirectUrl = client.auth_code.authorize_url(:redirect_uri => 'http://rubywebservicesample-en-qpqnydpsdm.elasticbeanstalk.com/oauth/callback', :scope => "read_account")
		redirect_to redirectUrl
		#redirect_to "https://oleadmin.ucc.nau.edu/api/oauth2/auth?response_type=code&client_id=ABCDCLIENTID&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Foauth%2Fcallback&scope=read_account"
	end
  end
end
